//
//  Router.swift
//  TheMovieDB
//
//  Created by Carolina Arcos on 12/12/17.
//  Copyright © 2017 Condor labs. All rights reserved.
//

import Foundation
import Alamofire


enum Router: URLRequestConvertible {
    
    case getPopular
    case getMovie(movieId: Int)
    case getTrailer(movieId: Int)
    
    static let baseURLString = "https://api.themoviedb.org/3/movie"
    
    var apiKey: String {
        get {
            return UserDefaults.standard.string(forKey: "TheMovieDbApiKey")!
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "TheMovieDbApiKey")
        }
    }
    
    var path: String {
        switch self {
        case .getPopular:
            return "/popular?api_key="
        //TODO: save api_key in keychain
        case .getMovie(let movieId):
            return "/\(movieId)?api_key="
        case .getTrailer(let movieId):
            return "/\(movieId)/videos?api_key="
        }
    }
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Router.baseURLString + path + apiKey)
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        return urlRequest
    }
}
