//
//  ResultCell.swift
//  TheMovieDB
//
//  Created by Prats on 10/12/17.
//  Copyright © 2017 Condor labs. All rights reserved.
//

import UIKit
import AlamofireImage

class ResultCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Public methods
    
    func configure(for result: Movie) {
        nameLabel.text = result.title
        voteAverageLabel.text = String(result.vote_average)
        
        let url = "http://image.tmdb.org/t/p/w185//\(result.poster_path)"
        if let image = URL(string: url) {
            posterImageView.af_setImage(withURL: image, placeholderImage: UIImage(named: "Placeholder"))
        } else {
            posterImageView.image = UIImage(named: "Placeholder")
        }
    }

}
