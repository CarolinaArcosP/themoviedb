//
//  OverviewTableViewCell.swift
//  TheMovieDB
//
//  Created by Carolina Arcos on 12/12/17.
//  Copyright © 2017 Condor labs. All rights reserved.
//

import UIKit

class OverviewTableViewCell: UITableViewCell {

    @IBOutlet weak var overviewLabel: UILabel!

}
