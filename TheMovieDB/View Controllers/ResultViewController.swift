//
//  ViewController.swift
//  TheMovieDB
//
//  Created by Prats on 10/12/17.
//  Copyright © 2017 Condor labs. All rights reserved.
//

import UIKit
import Alamofire

class ResultViewController: UIViewController, DetailViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var movies = [Movie]()
    
    struct TableViewCellIdentifiers {
        static let resultCell = "ResultCell"
        static let nothingFoundCell = "NothingFoundCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = 80
        
        //Result cell register
        var cellNib = UINib(nibName: TableViewCellIdentifiers.resultCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.resultCell)
        
        //Nothing found cell register
        cellNib = UINib(nibName: TableViewCellIdentifiers.nothingFoundCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.nothingFoundCell)
        
        getMovies()
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Detail view controller delegate
    
    func detailViewControllerDidCancel(_ controller: DetailViewController) {
        navigationController?.popViewController(animated: true)
    }
}

extension ResultViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if movies.count == 0 {
            return 1
        } else {
            return movies.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if movies.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.nothingFoundCell, for: indexPath)
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell", for: indexPath) as! ResultCell
            let result = movies[indexPath.row]
            cell.configure(for: result)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "MovieDetail", sender: indexPath)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MovieDetail" {
            let controller = segue.destination as! DetailViewController
            let indexPath = sender as! IndexPath
            let movie = movies[indexPath.row]
            controller.delegate = self
            controller.movie = movie
        }
    }
    
    // MARK: - Private Methods
    
    func showNetworkError(error: NSError) {
        let alert = UIAlertController(title: "O_O", message: "\(error)", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func getMovies() {
        let queue = DispatchQueue.global()
        //TODO: catch errors
        //print(Router.getPopular.url)
        Alamofire.request(Router.getPopular).responseJSON(queue: queue) { response in
//            print("Request: \(String(describing: response.request))")   //original url request
//             print("Response: \(String(describing: response.response?.statusCode))") //http url response
//             print("Result: \(String(describing: response.result))")                        // response result
//
//
//             if let json = response.result.value {
//             print("JSON: \(json)")
//             }
//
//             if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//             print("Data: \(utf8Text)")
//             }
            
            switch response.result {
            case .success:
                if let data = response.data {
                    self.movies = self.parse(data: data) //get results
                    self.movies.sort(by: {res1, res2 in
                        return res1.vote_average > res2.vote_average
                    })
                    print(self.movies)
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            case .failure(let error):
                print("Error: \(error)")
                //TODO: showNetworkError(error: error)
            }
        }
    }
    
    func parse(data: Data) -> [Movie] {
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(Result.self, from: data)
            print(result.total_results)
            return result.results
        } catch {
            print("JSON Error: \(error)")
            return []
        }
    }
    
}

