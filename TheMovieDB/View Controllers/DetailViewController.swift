//
//  DetailViewController.swift
//  TheMovieDB
//
//  Created by Carolina Arcos on 12/11/17.
//  Copyright © 2017 Condor labs. All rights reserved.
//

import UIKit
import Alamofire

protocol DetailViewControllerDelegate: class {
    func detailViewControllerDidCancel(_ controller: DetailViewController)
}

class DetailViewController: UITableViewController {

    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var budgetLabel: UILabel!
    @IBOutlet weak var tailerLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    weak var delegate: DetailViewControllerDelegate?
    var movie: Movie!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if movie != nil {
            title = movie.title
            updateInfo()
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateInfo() {
        //title = movie.title
        overviewLabel.text = movie.overview
        releaseDateLabel.text = movie.release_date
        
        //Add budget and trailer
        getMovieInfo()
        
        //TODO: add trailer
    }
    
    // MARK: - Private methods
    
    func getMovieInfo() {
        let queue = DispatchQueue.global()
        Alamofire.request(Router.getMovie(movieId: movie.movie_id)).responseJSON(queue: queue) { response in
            switch response.result {
            case .success:
                if let data = response.data {
                    if let result = self.parse(data: data) {
                        self.movie.budget = result.budget
                        self.movie.has_video = result.has_video
                    }
                }
                DispatchQueue.main.async {
                    self.budgetLabel.text = String(self.movie.budget ?? 0)
                }
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
    
    func getTrailer() {
        let queue = DispatchQueue.global()
        Alamofire.request(Router.getTrailer(movieId: movie.movie_id)).responseJSON(queue: queue) { response in
            switch response.result {
            case .success:
                if let data = response.data {
                    //TODO get videos
                }
            case .failure(let error):
                print("Trailer Error: \(error)")
            }
        }
    }
    
    func parse(data: Data) -> Movie? {
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(Movie.self, from: data)
            return result
        } catch {
            print("JSON Error: \(error)")
            return nil
        }
    }
    
    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
