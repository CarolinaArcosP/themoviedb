//
//  Movie.swift
//  TheMovieDB
//
//  Created by Prats on 10/12/17.
//  Copyright © 2017 Condor labs. All rights reserved.
//

import Foundation

struct Result: Codable {
    var total_results = 0
    var results = [Movie]()
}

struct Movie: Codable, CustomStringConvertible {
    var movie_id = 0
    var title = ""
    var vote_average = 0.0
    var poster_path = ""
    var overview = ""
    var release_date = ""
    var has_video = false
    var budget: Int?
    var trailer: String? // -->https://www.youtube.com/watch?v={video_key}
    
    enum CodingKeys: String, CodingKey {
        case movie_id = "id"
        case has_video = "video"
        case title, vote_average, poster_path
        case overview, release_date, budget, trailer
    }
    
    var description: String {
        return "\(movie_id): \(title), vote average: \(vote_average), video: \(has_video), trailer:"
    }
}
